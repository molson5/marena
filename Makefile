CC=gcc
CFLAGS=-g -O2 -I/home/marena/libjemalloc/include
LDFLAGS=-ljemalloc -lpthread -ldl -L/home/marena/libjemalloc/lib -lnuma
SOURCES=marena.c
OBJECTS=marena.o
MARENA=marena

LIBCFLAGS=-g -O2 -I/home/marena/libjemalloc/include
LIBLDFLAGS=-ljemalloc -lpthread -ldl -fPIC -shared -ldl -L/home/marena/libjemalloc/lib -lnuma
LIBSOURCES=libmarena.c
LIBOBJECTS=libmarena.o
LIBMARENA=libmarena.so

all: $(LIBMARENA) $(MARENA)

$(LIBMARENA): $(LIBSOURCES)
	$(CC) $(LIBCFLAGS) $(LIBLDFLAGS) $< -o $@

$(MARENA): $(OBJECTS)
	$(CC) $(CFLAGS) $(LDFLAGS) $< -o $@

$(OBJECTS): $(SOURCES)
	$(CC) $(LDFLAGS) $(CFLAGS) -c $<

clean:
	rm -f *.o $(LIBMARENA) $(MARENA)
