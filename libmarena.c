#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <errno.h>
#include <string.h>
#include <execinfo.h>
#include <limits.h>
#include <jemalloc/jemalloc.h>
#include <numa.h>
#include <numaif.h>
#include <pthread.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <unistd.h>
#include "uthash.h"

unsigned long totlibcsize = 0;

#define KB(n) n << 10
#define MB(n) n << 20
#define GB(n) n << 30
#define MAXSTR 512
#define FULL_CXT_SIZE 10
#define UNUSED_CXT 2
#define CXT_SIZE (FULL_CXT_SIZE - UNUSED_CXT)
#define MAX_CXT_STRLEN (CXT_SIZE*MAXSTR)
#define INVALID_ARENA UINT_MAX

unsigned hot_idx  = 0;
unsigned cold_idx = 0;

struct arena_node_info {
    unsigned long *nodemask;
    unsigned long maxnode;
    int mode;
};
struct arena_node_info *hot_node_info = NULL;
struct arena_node_info *cold_node_info = NULL;
unsigned long pagesize;

int malloc_cnt = 0;
int top_cxt_id = 0;
static int cnt = 0;
int disable_lookup = 0;
int per_phase_hotvecs = 0;
int profiling = 0;

extern void *__libc_malloc(size_t size);
extern void *__libc_calloc(size_t nmemb, size_t size);
extern void *__libc_realloc(void *ptr, size_t size);
extern void __libc_free(void *ptr);

void *marena_malloc(size_t size, unsigned arena_id);
void *marena_calloc(size_t nmemb, size_t size, unsigned arena_id);
void *marena_realloc(void *ptr, size_t size, unsigned arena_id);
void marena_free(void *ptr);

int __attribute__ ((constructor)) marena_init(void);
int __attribute__ ((destructor))  marena_exit(void);

FILE *malloc_stats_fp;

struct marena_active_s {
  pthread_t key; /* Thread ID is the key */
  char active; /* If marena is active or not in this thread */
  UT_hash_handle hh;
};

char marena_active_init = 0;
struct marena_active_s *marena_active = NULL;
pthread_rwlock_t marena_active_lock = PTHREAD_RWLOCK_INITIALIZER;
pthread_rwlock_t hash_mutex = PTHREAD_RWLOCK_INITIALIZER;

struct alloc_point_info {
    int id;
    unsigned arena_id;
    //unsigned hot_phases;
    UT_hash_handle hh;
};

struct hot_ap_entry {
    char cxt_strings[MAX_CXT_STRLEN];
    UT_hash_handle hh;
    unsigned arena_idx;
    unsigned hot_phases;
};

struct hot_ap_entry *hot_aps = NULL;

struct ptr_key {
  void *key;
  size_t size;
  struct alloc_point_info *ap_info;
  UT_hash_handle hh;
};

struct ptr_key *ptr_records = NULL;

struct hotvec_info {
  unsigned key;
  unsigned aid;
  UT_hash_handle hh;
};

struct hotvec_info *hotvec_arena_hash = NULL;

/* An array storing which arena each allocation point goes to */
int *alloc_point_arena = NULL;
size_t alloc_point_arena_size = 0;

/* Extent hooks for jemalloc */
extent_hooks_t *arena_extent_hooks = NULL;

char get_marena_active()
{
    struct marena_active_s *ret = NULL;
    pthread_t pid = pthread_self();
    int retval;
    //printf("get_marena_active\n"); fflush(stdout);

    /* Find if the thread has already been added to the hash */
    retval = pthread_rwlock_rdlock(&marena_active_lock);
    //printf("get_marena_active\n"); fflush(stdout);
    if(retval != 0) {
        printf("Fail: %d\n", retval);
        exit(retval);
    }
    HASH_FIND(hh, marena_active, &pid, sizeof(pthread_t), ret);
    pthread_rwlock_unlock(&marena_active_lock);

    //printf("Looked for thread\n"); fflush(stdout);

    if(ret == NULL) {
        //printf("Didn't find thread\n"); fflush(stdout);
        ret = (struct marena_active_s *) malloc(sizeof(struct marena_active_s));
        ret->key = pid;

        /* Add the structure to the hash */
        retval = pthread_rwlock_wrlock(&marena_active_lock);
        if(retval != 0) {
            printf("Failed to acquire read/write lock for marena_active\n");
            exit(retval);
        }
        HASH_ADD(hh, marena_active, key, sizeof(pthread_t), ret);
        pthread_rwlock_unlock(&marena_active_lock);

        ret->active = marena_active_init;
        return marena_active_init;
    }
    return ret->active;
}

void set_marena_active(char val)
{
    int retval;
    struct marena_active_s *ret = NULL;
    pthread_t pid = pthread_self();

    retval = pthread_rwlock_rdlock(&marena_active_lock);
    if(retval != 0) {
        printf("Failed to acquire read/write lock for marena_active 2\n");
        exit(retval);
    }
    HASH_FIND(hh, marena_active, &pid, sizeof(pthread_t), ret);
    pthread_rwlock_unlock(&marena_active_lock);

    if(ret == NULL) {
        ret = (struct marena_active_s *) __libc_malloc(sizeof(struct marena_active_s));
        ret->key = pid;

        retval = pthread_rwlock_wrlock(&marena_active_lock);
        if(retval != 0) {
            printf("Failed to acquire read/write lock for marena_active 3\n");
            exit(retval);
        }
        HASH_ADD(hh, marena_active, key, sizeof(pthread_t), ret);
        pthread_rwlock_unlock(&marena_active_lock);
    }
    ret->active = val;
}

void bind_ptr(void *ptr)
{
    int retval;
    struct ptr_key *pk = NULL;
    struct arena_node_info *node_info;

    /* Find the pointer in the hash table */
    retval = pthread_rwlock_rdlock(&hash_mutex);
    if(retval != 0) {
        printf("Failed to acquire read/write lock for hash_mutex\n");
        exit(retval);
    }
    HASH_FIND_PTR(ptr_records, &ptr, pk);
    pthread_rwlock_unlock(&hash_mutex);

    if(pk != NULL) {
        //printf("Found it in the hash table.\n"); fflush(stdout);
        /* First figure out if it's a hot or cold arena */
        if(pk->ap_info->arena_id == 1) {
          node_info = hot_node_info;
        } else {
          node_info = cold_node_info;
        }

        //printf("Binding to %lu\n", *(node_info->nodemask));
        if(mbind(((unsigned long)ptr) & ~((pagesize)-1), pk->size, node_info->mode, node_info->nodemask, node_info->maxnode, MPOL_MF_MOVE) != 0) {
          if(errno == EFAULT) {
            printf("EFAULT\n");
          } else if(errno == EINVAL) {
            printf("EINVAL\n");
          } else if(errno == EIO) {
            printf("EIO\n");
          }
        }
    } else {
      printf("Didn't find pointer in the hash table. NOT binding.\n"); fflush(stdout);
    }
}

void bind_arena(unsigned arena_id)
{
    int retval;
    struct ptr_key *iter, *tmp;
    struct arena_node_info *node_info;

    /* First figure out if it's a hot or cold arena */
    if(arena_id == hot_idx) {
      node_info = hot_node_info;
    } else {
      node_info = cold_node_info;
    }

    /* Iterate over all pointers that have been allocated,
     * check if they're in the target arena
     */
    retval = pthread_rwlock_rdlock(&hash_mutex);
    if(retval != 0) {
        printf("Failed to acquire read/write lock for hash_mutex 2\n");
        exit(retval);
    }
    HASH_ITER(hh, ptr_records, iter, tmp) {
        if(iter->ap_info->arena_id == arena_id) {
            mbind(iter->key, iter->size, node_info->mode, node_info->nodemask, node_info->maxnode, MPOL_MF_MOVE);
        }
    }
    pthread_rwlock_unlock(&hash_mutex);
}

struct alloc_point_info *get_alloc_point_info(int ap_id)
{
    struct alloc_point_info *ap_info = NULL;

    /* APH - Disabled by -n in the harness for overhead measurement */
    if (disable_lookup) {
        return NULL;
    }

    if (ap_info == NULL) {
        ap_info = malloc(sizeof(struct alloc_point_info));
        if (ap_info == NULL) {
            printf("error: could not allocate alloc_point_info\n");
            exit(ENOMEM);
        }

        ap_info->id = ap_id;
        if(alloc_point_arena_size > ap_id) {
          ap_info->arena_id = alloc_point_arena[ap_id];
        } else {
          printf("Can't find an entry for %d.\n", ap_id);
          exit(1);
        }
    }

    return ap_info;
}

/* get_arena_id: given an alloc_point_info structure, return the arena to
 * allocate new objects from. If ap_info is NULL, return the hot_idx
 */
unsigned get_arena_id(struct alloc_point_info *ap_info) {
    if (!ap_info) {
        return hot_idx;
    } 
    if(ap_info->arena_id == 1) {
      return hot_idx;
    } else {
      return cold_idx;
    }
}

void marena_bound(void *ptr) {
#if 0
    fprintf(stderr, "marena_bound: %lu %lu\n", (((intptr_t)ptr)>>12), ptr);
#endif
}

/* MRJ -- The marena_bound function communicates to Pin the expected bounds of
 * each arena. There is certainly a better way to do this. Consider using the
 * end symbol available with etext
 */
void do_marena_bound(unsigned aid) {
    int err;
    void *ptr;

    ptr = mallocx(sizeof(int), MALLOCX_ARENA(aid));

    marena_bound(ptr);

    dallocx(ptr, 0);
}

void add_to_ptr_records(void *ptr, size_t size, struct alloc_point_info *ap_info) {
    struct ptr_key *pk = NULL;

    if (pthread_rwlock_rdlock(&hash_mutex) != 0) fprintf(stderr, "can't get rdlock");
    HASH_FIND_PTR(ptr_records, &ptr, pk);
    pthread_rwlock_unlock(&hash_mutex);

    if (pk != NULL) {
        /* there is already a record so return */
        return;
    }

    pk = (struct ptr_key*)malloc(sizeof(struct ptr_key));
    if (pk == NULL) {
        printf("Failed to allocate memory.\n");
        exit(ENOMEM);
    }

    if (pthread_rwlock_wrlock(&hash_mutex) != 0) fprintf(stderr, "can't get wrlock");
    pk->key = ptr;
    pk->size = size;
    pk->ap_info = ap_info;

    HASH_ADD_PTR(ptr_records, key, pk);
    pthread_rwlock_unlock(&hash_mutex);
}

void *arena_extent_alloc(extent_hooks_t *extent_hooks,
    void *new_addr,
    size_t size,
    size_t alignment,
    bool *zero,
    bool *commit,
    unsigned arena_ind)
{
    printf("Allocating a chunk of %d bytes.\n", size); fflush(stdout);
    int err;
    void *addr = NULL;
    struct arena_node_info *node_info;

    addr = mmap(new_addr, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (addr == MAP_FAILED) {
        return NULL;
    }

    /* Bind the memory */
    if(arena_ind == hot_idx) {
      node_info = hot_node_info;
    } else {
      node_info = cold_node_info;
    }

    if(mbind(((unsigned long)addr) & ~((pagesize)-1), size, node_info->mode, node_info->nodemask, node_info->maxnode, MPOL_MF_MOVE) != 0) {
        if(errno == EFAULT) {
          printf("EFAULT\n");
        } else if(errno == EINVAL) {
          printf("EINVAL\n");
        } else if(errno == EIO) {
          printf("EIO\n");
        }
    }

    *zero = 1;
    *commit = 1;

    return addr;
}

bool arena_extent_dalloc(extent_hooks_t *extent_hooks,
    void *addr,
    size_t size,
    bool committed,
    unsigned arena_ind)
{
    return 1;
}

void arena_extent_destroy(extent_hooks_t *extent_hooks,
  void *addr,
  size_t size,
  bool committed,
  unsigned arena_ind)
{
    return 0;
}

bool arena_extent_commit(extent_hooks_t *extent_hooks,
    void *addr,
    size_t size,
    size_t offset,
    size_t length,
    unsigned arena_ind)
{
    return 0;
}

bool arena_extent_decommit(extent_hooks_t *extent_hooks,
    void *addr,
    size_t size,
    size_t offset,
    size_t length,
    unsigned arena_ind)
{
    return 1;
}

bool arena_extent_purge(extent_hooks_t *extent_hooks,
    void *addr,
    size_t size,
    size_t offset,
    size_t length,
    unsigned arena_ind)
{
    int err;

    err = madvise(addr + offset, length, MADV_DONTNEED);
    return (err != 0);
}

bool arena_extent_split(extent_hooks_t *extent_hooks,
    void *addr,
    size_t size,
    size_t size_a,
    size_t size_b,
    bool committed,
    unsigned arena_ind)
{
    return 0;
}

bool arena_extent_merge(extent_hooks_t *extent_hooks,
    void *addr_a,
    size_t size_a,
    void *addr_b,
    size_t size_b,
    bool committed,
    unsigned arena_ind)
{
    return 0;
}

void *marena_malloc(size_t size, unsigned arena_id) {
    int err;
    void *ptr;

    if (size == 0) {
        printf("Size is 0!\n"); fflush(stdout);
        return NULL;
    }

    ptr = mallocx(size, MALLOCX_ARENA(arena_id));

    return ptr;
}

void *marena_calloc(size_t nmemb, size_t size, unsigned arena_id) {
    int err;
    size_t my_size;
    void *ptr;

    if (nmemb == 0 || size == 0) {
        return NULL;
    }

    my_size = (nmemb*size);
    ptr = mallocx(my_size, MALLOCX_ARENA(arena_id));
    /* doing these accesses before doing xps_calloc_log will put the pages on
     * the wrong tier
     */
    //memset(ptr, 0, my_size);

    return ptr;
}

void *marena_realloc(void *ptr, size_t size, unsigned arena_id) {
    int err;

    /* earlier checks ensure ptr is non-null and size is non-zero */
    ptr = rallocx(ptr, size, MALLOCX_ARENA(arena_id));
    //bind_ptr(ptr);
    return ptr;
}

void marena_free(void *ptr) {
    int err;
    if (ptr) {
        dallocx(ptr, 0);
    }
}

void *ben_malloc(int ap_id, size_t size) {
    if(get_marena_active()) {
        void *ptr;
        struct alloc_point_info *ap_info;

        set_marena_active(0);
        
        ap_info = get_alloc_point_info(ap_id);

        ptr = marena_malloc(size, get_arena_id(ap_info));

        if (ptr != NULL) {
            add_to_ptr_records(ptr, size, ap_info);
            //xps_malloc_log(size, ptr, get_arena_idx(ap_info), ap_info);
        }
        //bind_ptr(ptr);
        
        set_marena_active(1);
        return ptr;

    }
    totlibcsize += size;
    return __libc_malloc(size);
}

void *ben_calloc(int ap_id, size_t nmemb, size_t size) {
    if (get_marena_active()) {
        void *ptr;
        struct alloc_point_info *ap_info;

        set_marena_active(0);
        ap_info = get_alloc_point_info(ap_id);
        ptr = marena_calloc(nmemb, size, get_arena_id(ap_info));
        if (ptr != NULL) {
            add_to_ptr_records(ptr, (nmemb*size), ap_info);
            //xps_calloc_log(nmemb, size, ptr, get_arena_id(ap_info), ap_info);
            memset(ptr, 0, (nmemb*size));
        }
        //bind_ptr(ptr);
        
        set_marena_active(1);
        return ptr;
    }
    totlibcsize += size;
    return __libc_calloc(nmemb, size);
}

void ben_free(void *ptr) {
    if (get_marena_active()) {
        struct ptr_key *pk;

        set_marena_active(0);
        pk = NULL;

        if(pthread_rwlock_rdlock(&hash_mutex) != 0) {
          fprintf(stderr, "Can't get rdlock\n");
          exit(1);
        }
        HASH_FIND_PTR(ptr_records, &ptr, pk);
        pthread_rwlock_unlock(&hash_mutex);

        if (pk == NULL) {
            free(ptr);
            set_marena_active(1);
            return;
        }

        if(pthread_rwlock_wrlock(&hash_mutex) != 0) fprintf(stderr, "can't get wrlock");
        HASH_DEL(ptr_records, pk);
        pthread_rwlock_unlock(&hash_mutex);

        free(pk);

        marena_free(ptr);
        //xps_free_log(ptr);

        set_marena_active(1);
        return;
    }
    __libc_free(ptr);
    return;
}

void *ben_realloc(int ap_id, void *ptr, size_t size) {
    if (get_marena_active()) {
        void *rptr;
        struct alloc_point_info *ap_info;
        struct ptr_key *pk;

        if (ptr == NULL) {
            return ben_malloc(ap_id, size);
        }

        if (size == 0) {
            ben_free(ptr);
            return NULL;
        }

        pk = NULL;
        HASH_FIND_PTR(ptr_records, &ptr, pk);
        if (pk == NULL) {
            return ben_malloc(ap_id, size);
        }

        set_marena_active(0);

        ap_info = get_alloc_point_info(ap_id);

        /* It is possible the previous allocation of this data is not in the
         * same arena as the realloc. In these cases, simply do a malloc in
         * the correct arena instead of a realloc
         */
        
        if (get_arena_id(pk->ap_info) != get_arena_id(ap_info)) {

            ptr = marena_malloc(size, get_arena_id(ap_info));
            if (ptr != NULL) {
                xps_malloc_log(size, ptr, get_arena_id(ap_info), ap_info);
                memcpy(ptr, pk->key, ((size < pk->size) ? size : pk->size));
                add_to_ptr_records(ptr, size, ap_info);

                marena_free(pk->key);
                //xps_free_log(pk->key);
                HASH_DEL(ptr_records, pk);

                free(pk);
            }

            set_marena_active(1);
            return ptr;
        }
        rptr = marena_realloc(ptr, size, get_arena_id(ap_info));
        if (rptr != NULL) {
            if (rptr != ptr) {
                struct ptr_key *pk = NULL;

                if (pthread_rwlock_rdlock(&hash_mutex) != 0) fprintf(stderr, "can't get rdlock");
                HASH_FIND_PTR(ptr_records, &ptr, pk);
                pthread_rwlock_unlock(&hash_mutex);

                if (pk != NULL) {
                    if (pthread_rwlock_wrlock(&hash_mutex) != 0) fprintf(stderr, "can't get wrlock");
                    HASH_DEL(ptr_records, pk);
                    pthread_rwlock_unlock(&hash_mutex);
                    free(pk);
                }

                /* add logic to free ptr because the data moved */
                add_to_ptr_records(rptr, size, ap_info);
            } else {
                HASH_FIND_PTR(ptr_records, &ptr, pk);
                pk->size = size;
            }
            xps_realloc_log(ptr, size, rptr, get_arena_id(ap_info), ap_info);
        }
        set_marena_active(1);
        return rptr;
    }
    return __libc_realloc(ptr, size);
}


void load_hot_aps(const char *hot_aps_file) {

    FILE *hotfp;
    char *lasts;
    int ap_id, arena_id, ret;

    printf("Reading in hot aps file.\n"); fflush(stdout);
    hotfp = fopen(hot_aps_file, "r");
    if (hotfp == NULL) {
        printf("error: could not open hot_aps_file: %s", hot_aps_file);
        exit(1);
    }
    
    fscanf(hotfp, "%*[^\n]\n");
    while(true) {
      ret = fscanf(hotfp, "%d%*[ ]%d%*[ ]%*s%*[ ]\n", &ap_id, &arena_id);
      if(ret == 2) {
        printf("Read in ap %d with arena %d.\n", ap_id, arena_id); fflush(stdout);
        alloc_point_arena = realloc(alloc_point_arena, sizeof(int) * (ap_id + 1));
        alloc_point_arena[ap_id] = arena_id;
        alloc_point_arena_size = ap_id + 1;
      } else {
        break;
      }
    }

    fclose(hotfp);
}

int marena_init() {
    size_t size;
    int err, ppid, i;
    char *nolookup, *hot_aps_file, *groupaps, *marena_profiling;
    FILE *fp;
    struct bitmask *hot_bitmask, *cold_bitmask;
    
    nolookup = getenv("MARENA_DISABLE_LOOKUP");
    if (nolookup != NULL && strcmp(nolookup, "True") == 0) {
        disable_lookup = 1;
    }
   
    groupaps = getenv("MARENA_GROUP_ALLOC_POINTS");
    if (groupaps != NULL && strcmp(groupaps, "True") == 0) {
        per_phase_hotvecs = 1;
    }

    marena_profiling = getenv("MARENA_PROFILING");
    if (marena_profiling != NULL && strcmp(marena_profiling, "True") == 0) {
        profiling = 1;
    }

    /* Allocate the extent hooks */
    arena_extent_hooks = (extent_hooks_t *)malloc(sizeof(extent_hooks_t));
    arena_extent_hooks->alloc = arena_extent_alloc;
    arena_extent_hooks->dalloc = arena_extent_dalloc;
    arena_extent_hooks->destroy = arena_extent_destroy;
    arena_extent_hooks->commit = arena_extent_commit;
    arena_extent_hooks->decommit = arena_extent_decommit;
    arena_extent_hooks->purge_lazy = arena_extent_purge;
    arena_extent_hooks->purge_forced = arena_extent_purge;
    arena_extent_hooks->split = arena_extent_split;
    arena_extent_hooks->merge = arena_extent_merge;

    /* Create the cold arena */
    size = sizeof(unsigned);
    if ((err = mallctl("arenas.create", &cold_idx, &size, NULL, 0)) != 0 ) {
        fprintf(stderr, "Error making cold arena: %d\n", err);
        return err;
    }

    /* Now attach the extent hooks to them */
    char cold_cmd[64];
    snprintf(cold_cmd, sizeof(cold_cmd), "arena.%u.extent_hooks", cold_idx);
    printf("Attaching extent hook: %s\n", cold_cmd); fflush(stdout);
    err = mallctl(cold_cmd, NULL, NULL, (void*)&arena_extent_hooks, sizeof(extent_hooks_t*));
    if(err) {
        fprintf(stderr, "Error making cold arena: %d\n", err);
        return err;
    }

    if (!per_phase_hotvecs) {
        /* Create the hot arena */
        size = sizeof(unsigned);
        if ((err = mallctl("arenas.create", &hot_idx, &size, NULL, 0)) != 0 ) {
            fprintf(stderr, "Error making hot arena: %d\n", err);
            return err;
        }

        char hot_cmd[64];
        snprintf(hot_cmd, sizeof(hot_cmd), "arena.%u.extent_hooks", hot_idx);
        err = mallctl(hot_cmd, NULL, NULL, (void*)&arena_extent_hooks, sizeof(extent_hooks_t*));
        if(err) {
            fprintf(stderr, "Error making hot arena: %d\n", err);
            return err;
        }
    }

    /* Fill in the NUMA node information about where you want the arenas */
    hot_node_info = malloc(sizeof(struct arena_node_info));
    hot_node_info->nodemask = malloc(sizeof(unsigned long));
    *(hot_node_info->nodemask) = 2;
    hot_node_info->maxnode = sizeof(unsigned long);
    hot_node_info->mode = MPOL_PREFERRED;

    cold_node_info = malloc(sizeof(struct arena_node_info));
    cold_node_info->nodemask = malloc(sizeof(unsigned long));
    *(cold_node_info->nodemask) = 1;
    cold_node_info->maxnode = sizeof(unsigned long);
    cold_node_info->mode = MPOL_PREFERRED;

    pagesize = sysconf(_SC_PAGESIZE);
     
    hot_aps_file = getenv("HOT_APS_FILE");
    if (hot_aps_file != NULL) {
        load_hot_aps(hot_aps_file);
    }
   
    marena_active_init = 1;
    set_marena_active(1);
    malloc_cnt = 0;
    top_cxt_id = 0;
    return 0;
}

void print_malloc_stats(void *cbopaque, const char *s) {
    fwrite(s, strlen(s), 1, malloc_stats_fp);
}

int marena_exit() {
    char *malloc_stats_file;
    int err;
    unsigned long totsize = 0;
    struct ptr_key *iter, *tmp;
    int retval;

    retval = pthread_rwlock_rdlock(&hash_mutex);
    if(retval != 0) {
        printf("Failed to acquire read/write lock.\n");
        exit(retval);
    }
    HASH_ITER(hh, ptr_records, iter, tmp) {
        totsize += iter->size;
    }
    pthread_rwlock_unlock(&hash_mutex);
    //printf("Total size in all arenas: %lu.\n", totsize);
    //printf("Total libc size: %lu.\n", totlibcsize);

    malloc_stats_file = getenv("MALLOC_STATS_FILE");
    if (malloc_stats_file != NULL) {

        malloc_stats_fp = fopen(malloc_stats_file, "w");

        if (malloc_stats_fp == NULL) {
            fprintf(stderr, "Error opening malloc_stats_file: %d\n", err);
            return err;
        }

        malloc_stats_print(print_malloc_stats,NULL,NULL);
        fclose(malloc_stats_fp);
    }
    return 0;
}
