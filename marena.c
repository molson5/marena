#define _GNU_SOURCE

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/time.h>

#define MAXSTR 512
#define LIBMARENA "/home/macslayer/projects/jemalloc/marena/libmarena.so"

static int verbose_flag     = 0;
static int lookup_flag      = 1;
static int groupaps_flag    = 0;
static char *time_file      = NULL;

static void should_not_reach_here() {
    abort();
}

void print_usage(char **argv) {
    fprintf(stderr, "marena is a simple wrapper program used to pass "
                    "arguments to the marena library\n"
                    "Usage: %s [-l LIBMARENA] [-f HOT_APS_FILE] [-t TIME_FILE] "
                    "[-np] -- prog\n", argv[0]); 
}

extern int opterr;

int main(int argc, char** argv)
{
    int c, need_usage, i, wstatus, exit_status, narenas;
    char *hot_aps_file, *malloc_stats_file, *libmarena;
    char jemalloc_conf_str[MAXSTR];
    pid_t cpid;
    struct timeval t1, t2;
    double elapsedtime;
    FILE* tfile;

    static struct option long_options[] =
    {
        /* These options set a flag. */
        {"verbose",   no_argument,       &verbose_flag, 1 },
        {"help",      no_argument,       0,            'h'},
        {"libmarena", required_argument, 0,            'l'},
        {"hotfile",   required_argument, 0,            'f'},
        {"statsfile", required_argument, 0,            's'},
        {"narenas",   required_argument, 0,            'r'},
        {"timefile",  required_argument, 0,            't'},
        {"nolookup",  no_argument,       0,            'n'},
        {"groupaps",  no_argument,       0,            'p'},
        {0, 0, 0, 0}
    };

    narenas = 1;

    hot_aps_file = malloc_stats_file = libmarena = NULL;
    c = need_usage = 0;
    while (1) {

        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long (argc, argv, "l:f:s:c:r:t:np",
                         long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
          break;

        switch (c) {

            case 0:
              /* If this option set a flag, do nothing else now. */
              if (long_options[option_index].flag != 0) {
              } else if (option_index == 2) {
                  libmarena = optarg;
              } else if (option_index == 3) {
                  hot_aps_file = optarg;
              } else if (option_index == 4) {
                  malloc_stats_file = optarg;
              } else if (option_index == 5) {
                  narenas = atoi(optarg);
              } else if (option_index == 6) {
                  time_file = optarg;
              }
              break;

            case 'l': libmarena         = optarg; break;
            case 'f': hot_aps_file      = optarg; break;
            case 's': malloc_stats_file = optarg; break;
            case 'r': narenas           = atoi(optarg); break;
            case 't': time_file         = optarg; break;
            case 'n': lookup_flag       = 0;      break;
            case 'p': groupaps_flag     = 1;      break;
            case 'h': need_usage        = 1;      break;
            case '?': need_usage        = 1;      break;
            default : should_not_reach_here();
        }
    }

    exit_status = 1;
    if (need_usage) {
        print_usage(argv);
        return exit_status;
    }

    if (verbose_flag) { 
        printf("libmarena: %s\n", libmarena);
        printf("hotfile: %s\n", hot_aps_file);
        printf("statsfile: %s\n", malloc_stats_file);
        printf("command:\n");
        for (i = optind; i < argc; i++) {
            printf("[%s]\n", argv[i]);
        }
    }

    // Launch the program under test
    cpid = fork();
    if (cpid == 0) {

        if (libmarena) {
            setenv("LD_PRELOAD", libmarena, 1);
        } else {
            setenv("LD_PRELOAD", LIBMARENA, 1);
        }

        if (hot_aps_file) {
            setenv("HOT_APS_FILE", hot_aps_file, 1);
        }

        if (!lookup_flag) {
            setenv("MARENA_DISABLE_LOOKUP", "True", 1);
        }

        if (groupaps_flag) {
            setenv("MARENA_GROUP_ALLOC_POINTS", "True", 1);
        }

        if (malloc_stats_file) {
            setenv("MALLOC_STATS_FILE", malloc_stats_file, 1);
        }

        setenv("MALLOC_CONF", jemalloc_conf_str, 1);

        execvp(argv[optind], &argv[optind]);
        perror(argv[optind]);
        exit(exit_status);

    } else {
        if (time_file) {
            gettimeofday(&t1, NULL);
        }

        if ( waitpid(cpid, &wstatus, 0) == -1) {
            perror("waitpid failed");
            exit(EXIT_FAILURE);
        };

        if(!WIFEXITED(wstatus)) {
            fprintf(stderr, "process terminated with signal: %d\n", WTERMSIG(wstatus));
        }
        
        if (time_file) {
            gettimeofday(&t2, NULL);
            elapsedtime =  t2.tv_sec * 1000000 + t2.tv_usec;
            elapsedtime -= t1.tv_sec * 1000000 + t1.tv_usec;
            
            tfile = fopen(time_file, "w");
            if (!tfile) {
                fprintf(stderr, "could not open time file for writing\n");
            } else {
                fprintf(tfile, "%f\n", elapsedtime);
                fclose(tfile);
            }
        }

        exit_status = WEXITSTATUS(wstatus);
    }

    return exit_status;
}
